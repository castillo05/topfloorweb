import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public token;
  public identity;

  constructor(private userService: UserService, private route: Router) {
    this.token = this.userService.getToken();
    this.identity= this.userService.getIdentity();

    if(!this.identity){
      this.route.navigate(['/login']);
    }

   }

  ngOnInit() {
  }

}
